require 'fileutils'

module Instamatic
  class SetupInstamaticUtil
    def self.copy_to_project!
      puts "Generate MVP Photo Placement Site?\nType ['Y/y'] to proceed\nType anything else to cancel"
      proceed = gets.chomp

      if proceed.downcase == 'y'
        copy_template_and_image_files
        puts "\nAll done!"
        puts "Add png files with transparency in the images folder and run the server to get started\n\n"
      end
    end

    def self.copy_template_and_image_files
      project_pwd = FileUtils.pwd
      lib_path = File.expand_path('../../lib', __FILE__)

      # TODO use this for a full generate-everything setup
      # FileUtils.copy_entry(lib_path, project_pwd) # this copies everything in lib

      templates_source = "#{lib_path}/instamatic/views/"
      templates_dest = "#{project_pwd}/templates"

      #puts "#{Dir.glob("#{templates_source}/*")}"

      FileUtils.cp_r(templates_source, templates_dest)

      images_source = "#{lib_path}/instamatic/images/"
      images_dest = "#{project_pwd}/images"

      FileUtils.cp_r(images_source, images_dest)

      config_ru_path = "#{File.expand_path('../..', __FILE__)}/config.ru"
      config_ru_dest = "#{project_pwd}/config.ru"
      
      puts "config_ru_path: #{config_ru_path}"
      puts "config_ru_dest: #{config_ru_dest}"

      FileUtils.cp_r(config_ru_path, config_ru_dest)
    end
  end
end
