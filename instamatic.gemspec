# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'instamatic/version'

Gem::Specification.new do |spec|
  spec.name          = 'instamatic'
  spec.version       = Instamatic::VERSION
  spec.authors       = ['Andrew Neely']
  spec.email         = ['aneely@gmail.com']

  spec.summary       = %q{It's an image placement service in a box!}
  spec.description   = %q{It's an image placement service in a box! Step 1: Install and run a generator. Step 2) Drop your images into the images directory. Step 3) Profit!}
  spec.homepage      = 'https://bitbucket.org/aneely/instamatic'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://rubygems.org"
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = ['setup_instamatic'] #spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib', 'utils']

  spec.add_development_dependency 'bundler', '~> 1.11'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_dependency 'sinatra', '1.4.5'
  spec.add_dependency 'mini_magick'
end
