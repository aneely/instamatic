# Instamatic

[![Gem Version](https://badge.fury.io/rb/instamatic.svg)](https://badge.fury.io/rb/instamatic)

Instamatic is a gem that lets you run a generator to create an image placement service in minutes.

Once your project is set up, just drop images (PNG files with transparency) into the `images/` folder and launch like any Sinatra app.

Add a config.ru file, and you're ready to push to heroku.

Requests to the root of your site land on an instruction page with a detailed explanation of how the URL scheme of the service works. 

Other URLs are parsed by the route matcher against the following two schemes. Note: all params but the first are optional in reverse order from the end.

TL;DR:

`/some_image_name/width/height/hex_color_w_no_leading_hash/scale/`

e.g. http://www.rappersonthe.rocks/l_l_cool_j/800/600/ffc0cb/

or

`/width/height/hex_color_w_no_leading_hash/scale/`
 
e.g. http://www.rappersonthe.rocks/640/480/000000/200/

A working example of the homepage and service is available at http://www.rappersonthe.rocks

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'instamatic'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install instamatic

## Usage

Getting started is easy!

1. After installing the gem, run `setup_instamatic` to get a prompt to generate the `images/` folder and a `templates/` folder with erb templates for the homepage to copy-edit. 
2. Drop your images into `images/` and run the server with `rackup`, `shotgun`, or `ruby`.
3. That's it!

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/aneely/instamatic.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Local dev notes

The .gitignore adds lines for .ruby-version and .ruby-gemset files. RVM and rbenv users may want to take note.